# Toolchain prefix (i.e arm-elf- -> arm-elf-gcc.exe)
TCHAIN_PREFIX = $(HOME)/toolchain/bin/arm-none-eabi-
REMOVE_CMD=rm
GDBTUI  = $(TCHAIN_PREFIX)gdb -tui

# Directory for output files (lst, obj, dep, elf, sym, map, hex, bin etc.)
OUTDIR = build

# Target file name (without extension).
TARGET = main

# Microcontroller model used for compiler-option (-mcpu)
MCU = cortex-m3


# Define programs and commands.
CC      = $(TCHAIN_PREFIX)gcc
CPP     = $(TCHAIN_PREFIX)g++
OBJCOPY = $(TCHAIN_PREFIX)objcopy
OBJDUMP = $(TCHAIN_PREFIX)objdump
NM      = $(TCHAIN_PREFIX)nm
REMOVE  = $(REMOVE_CMD) -f
STM32FLASH = ./stm32_flash.py

WORKSPACE_LOC=$(HOME)/workspace/FreeRTOS_hello_world
LDSCRIPT=src/gcc_arm.ld


CFLAGS = \
	-mthumb -gdwarf-2 -gstrict-dwarf \
	-O0 -g \
	-mcpu=$(MCU) \
	-D__STARTUP_CLEAR_BSS -DSTM32F100xB -DSTM32F1	\
	-I $(HOME)/toolchain/arm-none-eabi/include \
	-I $(WORKSPACE_LOC)/src/Inc \
	-I $(WORKSPACE_LOC)/src/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM3 \
	-I $(WORKSPACE_LOC)/src/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS \
	-I $(WORKSPACE_LOC)/src/Middlewares/Third_Party/FreeRTOS/Source/include \
	-I $(WORKSPACE_LOC)/src/Drivers/STM32F1xx_HAL_Driver/Inc \
	-I $(WORKSPACE_LOC)/src/Drivers/CMSIS/Include \
	-I $(WORKSPACE_LOC)/src/Drivers/CMSIS/Device/ST/STM32F1xx/Include \
	-mlong-calls -ffunction-sections -fdata-sections \
	-Wall -Wextra -Wcast-align -Wpointer-arith -Wredundant-decls -Wshadow   \
	-Wnested-externs -std=gnu99 \
	-Wa,-adhlns=$(addprefix $(OUTDIR)/, $(notdir $(addsuffix .lst, $(basename $<)))) \
	-MD -MP -MF $(OUTDIR)/dep/$(@F).d

# Linker flags.
#  -Wl,...:     tell GCC to pass this to linker.
#    -Map:      create map file
#    --cref:    add cross reference to  map file
LDFLAGS = \
	-mthumb -mcpu=$(MCU) \
	-nostartfiles -lc -lm -lgcc \
	-Wl,-Map=$(OUTDIR)/$(TARGET).map,-cref,--gc-sections \
	-T$(LDSCRIPT)


SOURCE=	\
    src/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c \
    src/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c \
    src/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c \
    src/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c \
    src/Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates/system_stm32f1xx.c \
    src/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c \
    src/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM3/port.c \
    src/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_1.c \
    src/Middlewares/Third_Party/FreeRTOS/Source/list.c \
    src/Middlewares/Third_Party/FreeRTOS/Source/tasks.c \
	src/startup_ARMCM3.c \
	src/main.c

# List of all source files without directory and file-extension
ALLSRC = $(notdir $(basename $(SOURCE)))

# Define all object files.
ALLOBJ     = $(addprefix $(OUTDIR)/, $(addsuffix .o, $(ALLSRC)))

# Define all listing files (used for make clean)
LSTFILES   = $(addprefix $(OUTDIR)/, $(addsuffix .lst, $(ALLSRC)))
# Define all depedency-files (used for make clean)
DEPFILES   = $(addprefix $(OUTDIR)/dep/, $(addsuffix .o.d, $(ALLSRC)))


.PHONY : all clean createdirs build elf hex bin lss sym flash ocd_run

all: createdirs build

elf: $(OUTDIR)/$(TARGET).elf
lss: $(OUTDIR)/$(TARGET).lss
sym: $(OUTDIR)/$(TARGET).sym
hex: $(OUTDIR)/$(TARGET).hex
bin: $(OUTDIR)/$(TARGET).bin


build: elf hex bin lss sym


$(ALLOBJ) : Makefile src/Inc/FreeRTOSConfig.h


# Create final output file (.hex) from ELF output file
%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

# Create final output file (.bin) from ELF output file
%.bin: %.elf
	$(OBJCOPY) -Obinary $< $@

# Create extended listing file/disassambly from ELF output file
# using objdump testing: option -C
%.lss: %.elf
	$(OBJDUMP) -h -S -C -r $< > $@

# Create a symbol table from ELF output file
%.sym: %.elf
	$(NM) -n $< > $@

# Link: create ELF output file from object files
.SECONDARY : $(TARGET).elf
.PRECIOUS : $(ALLOBJ)
%.elf:  $(ALLOBJ)
	$(CC) $(ALLOBJ) --output $@ $(LDFLAGS)


# Compile: create object files from C source files
define COMPILE_C_TEMPLATE
$(OUTDIR)/$(notdir $(basename $(1))).o : $(1)
	$(CC) -c $$(CFLAGS) $$(CONLYFLAGS) $$< -o $$@
endef
$(foreach src, $(SOURCE), $(eval $(call COMPILE_C_TEMPLATE, $(src))))


# Create output directories
createdirs:
	-@mkdir $(OUTDIR) 2>/dev/null || echo "" >/dev/null
	-@mkdir $(OUTDIR)/dep 2>/dev/null || echo "" >/dev/null

# Target: clean project
clean:
	$(REMOVE) $(OUTDIR)/$(TARGET).map
	$(REMOVE) $(OUTDIR)/$(TARGET).elf
	$(REMOVE) $(OUTDIR)/$(TARGET).hex
	$(REMOVE) $(OUTDIR)/$(TARGET).bin
	$(REMOVE) $(OUTDIR)/$(TARGET).sym
	$(REMOVE) $(OUTDIR)/$(TARGET).lss
	$(REMOVE) $(ALLOBJ)
	$(REMOVE) $(LSTFILES)
	$(REMOVE) $(DEPFILES)
	$(REMOVE) $(SOURCE:.c=.s)

# Startup ocd
ocd_run:
	openocd -f interface/jlink.cfg -f target/stm32f1x.cfg

#Flash the chip with the program
flash: bin
	$(STM32FLASH) --file $(OUTDIR)/$(TARGET).bin
