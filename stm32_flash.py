#! /usr/bin/python

import sys
import telnetlib
import getopt


def main():
    #Parse program arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hf:', ['help', 'file='])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    filename = 'main.bin'
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-f", "--file"):
            filename = a
        else:
            assert False, "unhandled option"

    #Check if the specified file exists
    try:
        with open(filename) as f: pass
        #print('File ' + filename + ' opened\n')
    except IOError:
        print('Error: File \'' + filename + '\' could not be opened\n')
        sys.exit(2)

    #Establish connection
    HOST = "localhost"
    PORT = 4444
    CONNECT_TIMEOUT = 1

    session = telnetlib.Telnet(HOST, PORT, CONNECT_TIMEOUT)

    try:
        #Conduct a session
        session.write("reset halt\n")
        session.write("flash probe 0\n")
        session.write("flash write_image erase " + filename + " 0x08000000\n")
        session.write("reset\n")
        session.write("exit\n")
        print(session.read_all())
    except:
        print('Ok...')


def usage():
    print('Usage: ' + sys.argv[0] + ' [options]\n' +
          '    -h, --help            give this help\n' +
          '    -f, --file\n' +
          '        file              binary file to flash\n');


if __name__ == "__main__":
    main()
